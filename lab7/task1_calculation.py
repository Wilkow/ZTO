from lab7.Problems import PermutationFlowShopProblemwithDelays as PSFP
from lab7.Algorithms import (
    get_front_pareto_and_list_simplified_simulated_annealing,
    get_hypervolume_point_pairs,
    get_hypervolume,
    random_permutation,
)

from itertools import chain
from statistics import mean
from multiprocessing import Pool, cpu_count

n = 40
instance = PSFP(n=n, Z=42)
targets = [instance.calc_makespan, instance.calc_max_tardiness]
init_sol = random_permutation(n)

maxiter = [100, 200, 400, 800, 1600]
N_repetitions = 10
hvi_list = {str(mit): [] for mit in maxiter}

for _ in range(N_repetitions):

    F_list, P_list = [], []
    with Pool(cpu_count()) as p:
        FP_list = p.starmap(
            get_front_pareto_and_list_simplified_simulated_annealing,
            [[targets, init_sol, m_iter] for m_iter in maxiter],
        )
    for lst in FP_list:
        F_list.append(lst[0])
        P_list.append(lst[1])

    p_nadir = [[tuple(target(p) for target in targets) for p in P] for P in P_list]
    z = (
        round(1.2 * max(list(chain(*p_nadir)), key=lambda x: x[0])[0]),
        round(1.2 * max(list(chain(*p_nadir)), key=lambda x: x[1])[1]),
    )

    for F, P, m_iter in zip(F_list, P_list, maxiter):

        p = [tuple(target(p) for target in targets) for p in P]
        p = list(dict.fromkeys(p))
        p = sorted(p, key=lambda x: x[0])

        f = [tuple(target(f) for target in targets) for f in F]
        f = list(dict.fromkeys(f))
        f = sorted(f, key=lambda x: x[0])

        hvi_list[str(m_iter)].append(int(get_hypervolume(f, z)))

print("iter", "\t", "HVI")
for k, v in hvi_list.items():
    print(k, "\t", mean(v))
