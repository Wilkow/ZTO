from Generator.RandomNumberGenerator import RandomNumberGenerator
import numpy as np
from math import floor


# Permutacyjny problem przepływowy dla 3 maszyn z żądanymi terminami zakończenia zadań
class PermutationFlowShopProblemwithDelays:
    def __init__(self, n, Z, m=3):
        self.n = n  # zadań
        self.m = m  # maszyn
        generator = RandomNumberGenerator(seedVaule=Z)
        self.p = np.ndarray((self.n, self.m))
        A = 0
        for i in range(m):
            for j in range(n):
                self.p[j, i] = generator.nextInt(1, 99)
                A = A + self.p[j, i]
        B = floor(A / 2)
        A = floor(A / 6)
        self.d = np.ndarray((self.n))
        for j in range(n):
            self.d[j] = generator.nextInt(A, B)

    def __calc_end_times(self, pi: list):
        C = np.ndarray((self.n, self.m))
        for j in range(self.n):
            for i in range(self.m):
                if i == 0 and j == 0:
                    C[pi[j], i] = self.p[pi[j], i]
                elif j > 0 and i == 0:
                    C[pi[j], i] = C[pi[j - 1], i] + self.p[pi[j], i]
                elif j == 0 and i > 0:
                    C[pi[j], i] = C[pi[j], i - 1] + self.p[pi[j], i]
                elif j > 0 and i > 0:
                    C[pi[j], i] = (
                        max(C[pi[j - 1], i], C[pi[j], i - 1]) + self.p[pi[j], i]
                    )
        return C

    def calc_makespan(self, pi: list):
        C = self.__calc_end_times(pi)
        return C[pi[self.n - 1], self.m - 1]

    def calc_total_flowtime(self, pi: list):
        C = self.__calc_end_times(pi)
        return sum(C[:, self.m - 1])

    def calc_max_tardiness(self, pi: list):
        C = self.__calc_end_times(pi)
        completion_delays = [
            max([0, C[j, self.m - 1] - self.d[j]]) for j in range(self.n)
        ]
        return max(completion_delays)

    def calc_total_tardiness(self, pi: list):
        C = self.__calc_end_times(pi)
        completion_delays = [
            max([0, C[j, self.m - 1] - self.d[j]]) for j in range(self.n)
        ]
        return sum(completion_delays)

    def calc_max_lateness(self, pi: list):
        C = self.__calc_end_times(pi)
        completion_delays = [C[j, self.m - 1] - self.d[j] for j in range(self.n)]
        return max(completion_delays)

    def calc_total_lateness(self, pi: list):
        C = self.__calc_end_times(pi)
        completion_delays = [C[j, self.m - 1] - self.d[j] for j in range(self.n)]
        return sum(completion_delays)
