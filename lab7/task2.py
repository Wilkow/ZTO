from lab7.Problems import PermutationFlowShopProblemwithDelays as PSFP
from lab7.Algorithms import (
    random_permutation,
    get_simplified_simulated_annealing_solution,
)
from functools import partial
from multiprocessing import Pool, cpu_count
from statistics import mean
import matplotlib.pyplot as plt
import numpy as np


def multicriteria_target(solution, coefficients: tuple[float], targets: list[callable]):
    return sum([c * target(solution) for c, target in zip(coefficients, targets)])


n = 40
instance = PSFP(n=n, Z=42)
targets = [
    instance.calc_makespan,
    instance.calc_max_tardiness,
    instance.calc_total_tardiness,
]
target_names = ["makespan", "max_tardiness", "total_tardiness"]
coefficients = [(0.3, 0.2, 0.4), (0.5, 0.5, 0.5), (0.2, 0.1, 0.8)]

maxiter = [100, 200, 400, 800, 1600]
N_repetitions = 10

results = {str(coeffs): {str(mit): [] for mit in maxiter} for coeffs in coefficients}

init_sol = random_permutation(n)

for coeffs in coefficients:
    target = partial(multicriteria_target, coefficients=coeffs, targets=targets)
    for _ in range(N_repetitions):
        with Pool(cpu_count()) as p:
            solutions = p.starmap(
                get_simplified_simulated_annealing_solution,
                [[target, init_sol, m_iter] for m_iter in maxiter],
            )

        for m_iter, sol in zip(maxiter, solutions):
            results[str(coeffs)][str(m_iter)].append(target(sol))

bar_width = round(0.95 / len(coefficients), 2)
for i, coefs in enumerate(coefficients):
    plt.bar(
        x=[x + i * bar_width for x in np.arange(len(maxiter))],
        height=[mean(m) for m in results[str(coefs)].values()],
        width=bar_width,
        label=str(coefs),
    )

plt.title(f"Optymalizacja po zagregowanym kryterium\n({", ".join(target_names)})")
plt.xlabel("maxiter")
plt.ylabel("zagregowane kryterium")
plt.xticks(
    [r + bar_width * (len(coefficients) - 1) / 2 for r in range(len(maxiter))], maxiter
)
plt.legend()
plt.show()
