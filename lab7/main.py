from lab7.Problems import PermutationFlowShopProblemwithDelays as PSFP
from numpy import random

n = 4
instance = PSFP(n=n, Z=42)
solution = list(range(0, n))
random.shuffle(solution)
solution = [0, 1, 2, 3]
print(solution)
print(instance.calc_makespan(solution))
print(instance.calc_total_flowtime(solution))
print(instance.calc_max_tardiness(solution))
print(instance.calc_total_tardiness(solution))
print(instance.calc_max_lateness(solution))
print(instance.calc_total_lateness(solution))
