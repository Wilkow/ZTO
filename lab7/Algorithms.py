import random


def random_permutation(n: int):
    permutation = list(range(0, n))
    random.shuffle(permutation)
    return tuple(permutation)


def swap(sol):
    swap_indexes = random.sample(range(len(sol)), 2)
    new_solution = [*sol]
    new_solution[swap_indexes[0]], new_solution[swap_indexes[1]] = (
        new_solution[swap_indexes[1]],
        new_solution[swap_indexes[0]],
    )
    return tuple(new_solution)


def insert(sol: list):
    reinsert_indexes = random.sample(range(len(sol)), 2)
    new_sol = [*sol]
    value = new_sol.pop(reinsert_indexes[0])
    new_sol.insert(reinsert_indexes[1], value)
    return tuple(new_sol)


def dominates(targets: list[callable], x1, x2) -> bool:
    dominates = False
    for target in targets:
        if target(x1) > target(x2):
            return False
        if target(x1) < target(x2):
            dominates = True
    return dominates


def get_front_pareto(targets: list[callable], P: list[list[int]]):
    F = [*P]
    for a in F[:]:
        for b in F[:]:
            if a != b:
                if dominates(targets, b, a):
                    F.remove(a)
                    break
    # Remove possible duplicates
    F = list(dict.fromkeys(F))
    return F


def get_front_pareto_and_list_simplified_simulated_annealing(
    targets: list[callable],
    init_sol,
    maxiter,
):
    x = tuple(init_sol)

    P = [x]
    it = 1
    while it < maxiter:

        x_new = insert(x)

        if dominates(targets, x_new, x):
            x = [*x_new]
            P.append(x_new)
        else:
            override_propability = 0.995**it
            if random.choices(
                [True, False], [override_propability, 1 - override_propability]
            )[0]:
                x = [*x_new]
                P.append(x_new)
        it += 1

    F = get_front_pareto(targets, P)
    return F, P


def get_hypervolume_point_pairs(pareto_front, nadir_point):
    point_pairs = []
    for i in range(len(pareto_front)):
        if i == 0:
            x_cords = (nadir_point[0], pareto_front[i][0])
            y_cords = (nadir_point[1], pareto_front[i][1])
        else:
            x_cords = (nadir_point[0], pareto_front[i][0])
            y_cords = (pareto_front[i - 1][1], pareto_front[i][1])
        point_pairs.append([(x_cords[0], y_cords[0]), (x_cords[1], y_cords[1])])
    return point_pairs


def get_hypervolume(pareto_front, nadir_point):
    point_pairs = get_hypervolume_point_pairs(pareto_front, nadir_point)
    hypervolume = 0
    for p1, p2 in point_pairs:
        hypervolume += (p1[0] - p2[0]) * (p1[1] - p2[1])
    return hypervolume


# Zadanie 2 algorytm
def get_simplified_simulated_annealing_solution(
    target: list[callable],
    init_sol: tuple[int],
    maxiter: int,
):
    x = tuple(init_sol)

    it = 1
    while it < maxiter:

        x_new = insert(x)

        if target(x_new) < target(x):
            x = [*x_new]
        else:
            override_propability = 0.995**it
            if random.choices(
                [True, False], [override_propability, 1 - override_propability]
            )[0]:
                x = [*x_new]
        it += 1

    return x
