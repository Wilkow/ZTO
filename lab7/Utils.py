from matplotlib.pyplot import Figure, Axes
from matplotlib import pyplot as plt


# Type hinting
def create_subplots() -> tuple[Figure, Axes]:
    return plt.subplots()


# Workaround for interactive legend with scatter plot
class ScatterLegendPicker:
    def __init__(self, fig: Figure, legend, chart_elements):
        self.map_legend_to_elements = {}
        self.fig: Figure = fig
        for legend, elements in zip(legend.get_lines(), chart_elements):
            legend.set_picker(5)
            self.map_legend_to_elements[legend] = elements

    def on_pick(self, event):
        legend_line = event.artist

        if legend_line not in self.map_legend_to_elements.keys():
            return

        new_visibility = not self.map_legend_to_elements[legend_line][0].get_visible()
        for element in self.map_legend_to_elements[legend_line]:
            element.set_visible(new_visibility)

        legend_line.set_alpha(1.0 if new_visibility else 0.2)
        self.fig.canvas.draw()
