from lab7.Problems import PermutationFlowShopProblemwithDelays as PSFP
from lab7.Algorithms import (
    get_front_pareto_and_list_simplified_simulated_annealing,
    get_hypervolume_point_pairs,
    get_hypervolume,
    random_permutation,
)

from itertools import chain
from statistics import mean
from multiprocessing import Pool, cpu_count
import random
import matplotlib.pyplot as plt
import numpy as np

n = 40
instance = PSFP(n=n, Z=42)
categories = ['makespan','max_tardiness','total_flowtime','total_tardiness']
targets = [instance.calc_makespan, instance.calc_max_tardiness, instance.calc_total_flowtime, instance.calc_total_tardiness]
init_sol = random_permutation(n)

maxiter = [1600]
N_repetitions = 1
vis_solutions = []

for _ in range(N_repetitions):

    F_list, P_list = [], []
    with Pool(cpu_count()) as p:
        FP_list = p.starmap(
            get_front_pareto_and_list_simplified_simulated_annealing,
            [[targets, init_sol, m_iter] for m_iter in maxiter],
        )

    f_len = -1
    while f_len != 3:
        F_, P_ = get_front_pareto_and_list_simplified_simulated_annealing(targets, init_sol, maxiter[0])
        f_len = len(F_)
    F_list.append(F_)
    P_list.append(P_)

    for F, P in zip(F_list, P_list):

        f = [tuple(target(f) for target in targets) for f in F]
        p = [tuple(target(p) for target in targets) for p in P]
        vis_solutions[:] = f
        vis_solutions.append(p[random.randint(0,len(P_list)-1)])
        
print(vis_solutions)

# Data
values = vis_solutions
new_values = [list(values) for values in values]
values = new_values[:]
max_val = [0,0,0,0]
for targets in values:
    for i,target_val in enumerate(targets):
        if target_val >= max_val[i]:
            max_val[i] = target_val
# print(max_val)
for i,_ in enumerate(values):
    for j, _ in enumerate(targets):
        values[i][j] = values[i][j] / max_val[j]

print(values)
# Plot
fig = plt.figure()
ax = fig.add_subplot(111, polar=True)
for vals in values:
    ax.plot(np.linspace(0, 2 * np.pi, len(categories), endpoint=False), vals)
    ax.fill(np.linspace(0, 2 * np.pi, len(categories), endpoint=False), vals, alpha=0.25)

# Set ticks
ax.set_xticks(np.linspace(0, 2 * np.pi, len(categories), endpoint=False))
ax.set_xticklabels(categories)

# Title
plt.title("Radar Chart")
plt.show()