from lab7.Problems import PermutationFlowShopProblemwithDelays as PSFP
from lab7.Utils import create_subplots, ScatterLegendPicker
from lab7.Algorithms import (
    get_front_pareto_and_list_simplified_simulated_annealing,
    get_hypervolume_point_pairs,
    get_hypervolume,
    random_permutation,
)

from itertools import chain
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.patches as patches
from multiprocessing import Pool, cpu_count

n = 40
instance = PSFP(n=n, Z=42)
targets = [instance.calc_makespan, instance.calc_max_tardiness]
init_sol = random_permutation(n)
init_point = tuple(target(init_sol) for target in targets)

maxiter = [100, 200, 400, 800, 1600]
color_pairs = [
    ("lightblue", "blue"),
    ("lightcoral", "red"),
    ("lightgreen", "green"),
    ("lightyellow", "yellow"),
    ("peachpuff", "orange"),
]

F_list, P_list = [], []
with Pool(cpu_count()) as p:
    FP_list = p.starmap(
        get_front_pareto_and_list_simplified_simulated_annealing,
        [[targets, init_sol, m_iter] for m_iter in maxiter],
    )
    for lst in FP_list:
        F_list.append(lst[0])
        P_list.append(lst[1])

fig, ax = create_subplots()
legend_entries, legend_labels, chart_elements = [], [], []

p_nadir = [[tuple(target(p) for target in targets) for p in P] for P in P_list]
z = (
    round(1.2 * max(list(chain(*p_nadir)), key=lambda x: x[0])[0]),
    round(1.2 * max(list(chain(*p_nadir)), key=lambda x: x[1])[1]),
)

print("iter", "\t", "HVI")
for F, P, m_iter, color_pair in zip(F_list, P_list, maxiter, color_pairs):

    p = [tuple(target(p) for target in targets) for p in P]
    p = list(dict.fromkeys(p))
    p = sorted(p, key=lambda x: x[0])

    f = [tuple(target(f) for target in targets) for f in F]
    f = list(dict.fromkeys(f))
    f = sorted(f, key=lambda x: x[0])

    pairs = get_hypervolume_point_pairs(f, z)
    print(m_iter, "\t", int(get_hypervolume(f, z)))

    p_legend = Line2D([0], [0], linestyle="none", color=color_pair[0], marker="o")
    p_scatter = ax.scatter(
        [point[0] for point in p],
        [point[1] for point in p],
        label="P",
        color=color_pair[0],
    )

    f_legend = Line2D([0], [0], linestyle="none", color=color_pair[1], marker="o")
    f_scatter = ax.scatter(
        [point[0] for point in f],
        [point[1] for point in f],
        label="F",
        color=color_pair[1],
        marker="o",
    )

    (f_line,) = ax.step(
        [point[0] for point in f],
        [point[1] for point in f],
        color=color_pair[1],
        where="post",
    )
    f_rects = []
    for pair in pairs:
        rect = patches.Rectangle(
            (pair[0][0], pair[0][1]),
            pair[0][0] - pair[1][0],
            pair[0][1] - pair[1][1],
            linewidth=0,
            facecolor=color_pair[1],
            alpha=0.10,
            angle=180,
        )
        ax.add_patch(rect)
        f_rects.append(rect)

    legend_entries.append(p_legend)
    chart_elements.append([p_scatter])
    legend_labels.append(f"P (maxiter={m_iter})")
    legend_entries.append(f_legend)
    chart_elements.append([f_scatter, f_line, *f_rects])
    legend_labels.append(f"F (maxiter={m_iter})")

ax.scatter(z[0], z[1], label="Z", color="black", marker="v")
ax.scatter(init_point[0], init_point[1], label="Z", color="black", marker="^")
legend = ax.legend(tuple(legend_entries), tuple(legend_labels), numpoints=1, loc="best")
legend.set_draggable(True)

slp = ScatterLegendPicker(fig, legend, chart_elements)
fig.canvas.mpl_connect("pick_event", slp.on_pick)
plt.xlabel("makespan")
plt.ylabel("max_tardiness")
plt.title("Wyznaczanie frontu Pareto")
plt.grid(True)
plt.show()
