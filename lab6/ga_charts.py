import pickle
from matplotlib import pyplot as plt
import numpy as np

def removekey(d, key):
    r = dict(d)
    del r[key]
    return r

algorithm_kwargs = {
    "selection_method": "tournament",
    "mutation_method": "swap",
    "mutate_prob": 0.02,
    "p": 500,
    "iter_limit": 100,
}

param_grid = {
    "selection_method": ["tournament", "roulette"],
    "mutation_method": ["reinsert", "swap"],
    "mutate_prob": [0.02, 0.05, 0.1],
    "p": [50, 100, 200, 500],
    "iter_limit": [100],
}

with open('lab6/saved_grid_params_results.pkl', 'rb') as f:
    loaded_dict = pickle.load(f)

for key in ['selection_method','mutation_method']:
    ax = plt.subplot()
    for method in param_grid[key]:
        for params, result in loaded_dict:
            values = []
            if params[key] == method and removekey(params, key) == removekey(algorithm_kwargs, key):
                values.append(result)
            ax.bar(method, values)
    plt.show()

plt.clf()
# for key in ['mutate_prob', 'p']:
#     ax = plt.plot()
#     for param_value in param_grid[key]:
#         for params, result in loaded_dict:
#             values = []
#             print(params[key], param_value, removekey(params, key), removekey(algorithm_kwargs, key))
#             if params[key] == param_value and removekey(params, key) == removekey(algorithm_kwargs, key):
#                 print(result)
#                 values.append(result)
#             plt.plot(param_grid[key],values)
#     plt.show()

