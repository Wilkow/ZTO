from lab6.GeneticAlgorithm import ga
from lab6.Problems import PermutationFlowShopProblem
import itertools
import numpy as np


param_grid = {
    "selection_method": ["tournament", "roulette"],
    "mutation_method": ["reinsert", "swap"],
    "mutate_prob": [0.02, 0.05, 0.1],
    "p": [50, 100, 200, 500],
    "iter_limit": [100]
}

instance = PermutationFlowShopProblem(
    n=20,  # liczba zadań
    m=5,  # liczba maszyn
    Z=42  # ziarno generatora
)

best_sol = None
best_result = np.inf
best_params = None

keys = list(param_grid.keys())
lists = list(param_grid.values())

combinations = list(itertools.product(*lists))
dict_combinations = [dict(zip(keys, combination)) for combination in combinations]

for num, params in enumerate(dict_combinations):
    print(f'{num+1}/{len(dict_combinations)}')
    print(params)
    sol = ga(instance=instance, **params)
    if instance.calc_target(sol) < best_result:
        best_sol = sol
        best_result = instance.calc_target(sol)
        best_params = params

print(best_sol)
print(best_result)
print(best_params)
