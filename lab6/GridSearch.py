from multiprocessing import Pool, cpu_count
from itertools import product
import numpy as np


class GridSearch:
    def __init__(
        self, params_grid: dict, n_repeats: int, func: callable, target: callable
    ):
        self.n_repeats = n_repeats
        self.func = func
        self.target = target

        # Create list of all kwargs combinations from params_grid
        keys = list(params_grid.keys())
        lists = list(params_grid.values())
        combinations = list(product(*lists))
        dict_combinations = [
            dict(zip(keys, combination)) for combination in combinations
        ]
        self.params_list = dict_combinations

    def _worker_wrapper(self, args):
        sum = 0
        for _ in range(self.n_repeats):
            sum += self.target(self.func(**args))
        return args, sum / self.n_repeats

    def run(self):
        with Pool(cpu_count()) as p:
            args_avg_results = p.map(self._worker_wrapper, self.params_list)

        best_avg_result = np.inf
        best_params = None

        for args, avg_res in args_avg_results:
            if avg_res < best_avg_result:
                best_avg_result = avg_res
                best_params = args

        return best_params, best_avg_result
    
    def run_return_all_resulsts(self):
        with Pool(cpu_count()) as p:
            args_avg_results = p.map(self._worker_wrapper, self.params_list)
        return args_avg_results
