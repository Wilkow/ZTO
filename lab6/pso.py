from lab6.ParticleSwarmOptimization import pso
from lab6.Problems import HimmelblauFunction
import numpy as np
import matplotlib.pyplot as plt
import numpy as np


# Funkcja problemu/celu wraz z ograniczeniami
target = HimmelblauFunction.calc_target
himmelblau_kwargs = {
    "n": 2,
    "target": target,
    "L": np.array(2 * [-5]),
    "U": np.array(2 * [5]),
}

# Parametry skopiowane bezpośrednio z wyniku przeszukania kratowego
algorithm_kwargs = {
    "iteration_limit": 100,
    "phi_l": 1,
    "phi_g": 1,
    "w": 0.9,
    "c": 0.25,
    "k": 500,
}

sol = pso(**himmelblau_kwargs, **algorithm_kwargs)

print(f"solution = {sol}")
print(f"f(solution) = {target(sol)}")

algorithm_kwargs_list = [
    {"iteration_limit": 100,
    "phi_l": 2,
    "phi_g": 1,
    "w": 0.9,
    "c": 0.4,
    "k": 500},
    {"iteration_limit": 100,
    "phi_l": 4,
    "phi_g": 1,
    "w": 0.9,
    "c": 0.4,
    "k": 500},
    {"iteration_limit": 100,
    "phi_l": 2,
    "phi_g": 1,
    "w": 0.9,
    "c": 0.9,
    "k": 500},
    {"iteration_limit": 100,
    "phi_l": 4,
    "phi_g": 1,
    "w": 0.9,
    "c": 0.9,
    "k": 500},
]

n_repetitions = 10
results = {}

for kwargs in algorithm_kwargs_list:
    key = (kwargs['phi_l'], kwargs['c'])
    values = [target(pso(**himmelblau_kwargs, **kwargs)) for _ in range(n_repetitions)]
    print(values)
    average_value = np.mean(values)
    results[key] = average_value

labels = [f"phi_l/c:\n{sel}\n{mut}" for sel, mut in results.keys()]
values = list(results.values())

print(values)
fig, ax = plt.subplots()
ax.bar(labels, values, color=['blue', 'orange', 'green', 'red'])
ax.set_xlabel('PSO Configuration')
ax.set_ylabel('Average Objective Value')
ax.set_title('Comparison of PSO Configurations')

plt.xticks(rotation=45)
plt.tight_layout()
plt.show()
