from Generator.RandomNumberGenerator import RandomNumberGenerator
import numpy as np


# 2.8 Permutacyjny problem przepływowy
class PermutationFlowShopProblem:
    def __init__(self, n, m, Z):
        self.n = n  # zadań
        self.m = m  # maszyn
        generator = RandomNumberGenerator(seedVaule=Z)
        self.p = np.ndarray((self.n, self.m))
        for i in range(n):
            for j in range(m):
                self.p[i, j] = generator.nextInt(1, 99)

    def calc_target(self, pi: list):
        C = np.ndarray((self.n, self.m))
        for j in range(self.n):
            for i in range(self.m):
                if i == 0 and j == 0:
                    C[pi[j], i] = self.p[pi[j], i]
                elif j > 0 and i == 0:
                    C[pi[j], i] = C[pi[j - 1], i] + self.p[pi[j], i]
                elif j == 0 and i > 0:
                    C[pi[j], i] = C[pi[j], i - 1] + self.p[pi[j], i]
                elif j > 0 and i > 0:
                    C[pi[j], i] = (
                        max(C[pi[j - 1], i], C[pi[j], i - 1]) + self.p[pi[j], i]
                    )
        return C[pi[self.n - 1], self.m - 1]


# 3.1 Himmelblau's function
class HimmelblauFunction:
    @staticmethod
    def calc_target(X):
        return (X[0] ** 2 + X[1] - 11) ** 2 + (X[0] + X[1] ** 2 - 7) ** 2
