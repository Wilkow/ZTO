from lab6.GeneticAlgorithm import ga
from lab6.Problems import PermutationFlowShopProblem
from lab6.GridSearch import GridSearch
from functools import partial
import pickle
# Instancja problemu
instance = PermutationFlowShopProblem(
    n=20, m=5, Z=42  # liczba zadań  # liczba maszyn  # ziarno generatora
)
target = instance.calc_target
func_grid = partial(ga, instance=instance)


algorithm_kwargs = {
    "selection_method": "tournament",
    "mutation_method": "swap",
    "mutate_prob": 0.02,
    "p": 500,
    "iter_limit": 100,
}

param_grid = {
    "selection_method": ["tournament", "roulette"],
    "mutation_method": ["reinsert", "swap"],
    "mutate_prob": [0.02, 0.05, 0.1],
    "p": [50, 100, 200, 500],
    "iter_limit": [100],
}


params_results = GridSearch(
    params_grid=param_grid, n_repeats=3, func=func_grid, target=target
).run_return_all_resulsts()

for params, result in params_results:
    print(params, result)


with open('lab6/saved_grid_params_results.pkl', 'wb') as f:
    pickle.dump(params_results, f)
