from lab6.Problems import PermutationFlowShopProblem
import numpy as np
import random
from time import time
from functools import partial
from typing import Literal


### Generacja populacji początkowej
def random_shuffle(n: int):
    seq = np.arange(n)
    np.random.shuffle(seq)
    return seq


def get_k_best_from_n(instance: PermutationFlowShopProblem, K: int, N: int) -> list:
    random_solutions = [random_shuffle(instance.n) for _ in range(N)]
    random_solutions = sorted(random_solutions, key=instance.calc_target)
    return random_solutions[:K]


def get_m_initial_solutions(
    instance: PermutationFlowShopProblem, M: int = 100, K: int = 20, N: int = 1000
):
    initial_solutions = [
        *get_k_best_from_n(instance, K, N),
        *[random_shuffle(instance.n) for _ in range(M - K)],
    ]
    np.random.shuffle(initial_solutions)
    return initial_solutions


### Krzyżowanie
# https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm) OX1
def ox1_cx(parent1, parent2, cx_points):
    p1_subset = parent1[cx_points[0] : cx_points[1]]
    remaining_p2 = [x for x in parent2 if x not in p1_subset]
    child = remaining_p2
    child[cx_points[0] : cx_points[0]] = p1_subset
    return child


def ox1_cx_pair(parent1, parent2):
    cx_points = tuple(sorted(random.sample(range(0, len(parent1) + 1), 2)))
    child1, child2 = ox1_cx(parent1, parent2, cx_points), ox1_cx(
        parent2, parent1, cx_points
    )
    return child1, child2


### Dobór rodziców
def calc_population_results(
    instance: PermutationFlowShopProblem, population: list[list]
) -> list:
    return [instance.calc_target(sol) for sol in population]


def roulette(
    instance: PermutationFlowShopProblem, population: list[list]
) -> list[tuple[list]]:
    parents = []
    results = calc_population_results(instance, population)
    results_sum = sum(results)
    # Odwrócenie dla problemu minimalizacji
    inverse_propabilities = [results_sum / r for r in results]
    inverse_propabilities_sum = sum(inverse_propabilities)
    propabilities = [
        inv_prob / inverse_propabilities_sum for inv_prob in inverse_propabilities
    ]
    size = round(len(population) / 2)
    for _ in range(size):
        indices = np.random.choice(
            len(population), replace=False, size=2, p=propabilities
        )
        parents.append((population[indices[0]], population[indices[1]]))
    return parents


def tournament(
    instance: PermutationFlowShopProblem, population: list[list], k: int
) -> list[tuple[list]]:
    parents = []
    results = calc_population_results(instance, population)
    size = round(len(population) / 2)
    # Odrazu wybieramy parę najlepszych wylosowanych w turnieju
    for _ in range(size):
        indices = np.random.choice(len(population), replace=False, size=k)
        selected_values = [results[i] for i in indices]
        best_indices = [indices[i] for i in np.argsort(selected_values)[:2]]
        parents.append((population[best_indices[0]], population[best_indices[1]]))
    return parents


### Możliwe operacje mutacji
def swap(sol: list):
    swap_indexes = random.sample(range(len(sol)), 2)
    new_sol = [*sol]
    new_sol[swap_indexes[0]], new_sol[swap_indexes[1]] = (
        new_sol[swap_indexes[1]],
        new_sol[swap_indexes[0]],
    )
    return new_sol


def reinsert(sol: list):
    reinsert_indexes = random.sample(range(len(sol)), 2)
    new_sol = [*sol]
    value = new_sol.pop(reinsert_indexes[0])
    new_sol.insert(reinsert_indexes[1], value)
    return new_sol


def ga(
    instance: PermutationFlowShopProblem,  # Instancja problemu
    p: int,  # Całkowita wielkość populacji
    mutate_prob: float,  # Prawdopodobieństwo mutacji
    selection_method: Literal["roulette", "tournament"],  # Metoda selekcji rodziców
    mutation_method: Literal["swap", "reinsert"],  # Metode mutacji
    time_limit: int = None,  # Limit czasowy jako warunek stopu
    iter_limit: int = None,  # Limit ilości iteracji od poprawy jako warunek stopu
    k: int = None,  # Ilość osobników wybieranych w losowaniu turniejowym
    N: int = 1000,  # Ilość osobników z których zostanie wybranych k najlepszych do początkowej populacji
    K: int = 20,  # Ilośc najlepszych osobników wybranych z N losowo wybranych
):
    # Domyślnie 5% populacji jest losowane w turnieju
    if selection_method == "tournament" and not k:
        k = round(0.05 * p)

    # Metody wyboru rodziców i mutacji
    if selection_method == "roulette":
        get_parents = partial(roulette, instance=instance)
    else:
        get_parents = partial(tournament, instance=instance, k=k)

    if mutation_method == "swap":
        mutate = swap
    else:
        mutate = reinsert

    target = instance.calc_target

    # Generacja populacji początkowej, wybrane zostaje K najlepszych
    # z N losowych  oraz dodatkowo M-K zupełnie losowych, łącznie M
    initial_population = get_m_initial_solutions(instance, M=p, N=N, K=K)
    initial_results = calc_population_results(instance, initial_population)

    index = np.argsort(initial_results)[0]
    best_x = initial_population[index]

    start_time = time()
    iter_count = 0
    X = initial_population

    while True:

        R = get_parents(population=X)
        C = []

        for r in R:
            c_1, c_2 = ox1_cx_pair(r[0], r[1])
            C.append(c_1)
            C.append(c_2)

        for i, c in enumerate(C):
            if target(c) < target(best_x):
                best_x = c
                iter_count = 0

            if random.choices([True, False], [mutate_prob, 1 - mutate_prob])[0]:
                C[i] = mutate(c)
                if target(C[i]) < target(best_x):
                    best_x = C[i]
                    iter_count = 0

        X = C

        if time_limit and time() - start_time > time_limit:
            break
        if iter_limit and iter_count > iter_limit:
            break

        iter_count += 1

    return best_x
