from lab6.ParticleSwarmOptimization import pso
from lab6.Problems import HimmelblauFunction
import numpy as np
from lab6.GridSearch import GridSearch
from functools import partial

# Funkcja problemu/celu wraz z ograniczeniami
target = HimmelblauFunction.calc_target
himmelblau_kwargs = {
    "n": 2,
    "target": target,
    "L": np.array(2 * [-5]),
    "U": np.array(2 * [5]),
}

# Parametry do przeszukania kratowego
func_grid = partial(pso, **himmelblau_kwargs)
param_grid = {
    "iteration_limit": [100],
    "phi_l": [0.1*i for i in range(1,30)],
    "phi_g": [1],
    "w": [0.9],
    "c": [0.1*i for i in range(1,10)],
    "k": [500],
}

best_params, best_avg_result = GridSearch(
    params_grid=param_grid, n_repeats=10, func=func_grid, target=target
).run()
print(best_params, best_avg_result)
