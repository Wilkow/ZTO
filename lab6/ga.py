from lab6.GeneticAlgorithm import ga
from lab6.Problems import PermutationFlowShopProblem
import matplotlib.pyplot as plt
import numpy as np


# Instancja problemu
instance = PermutationFlowShopProblem(
    n=20, m=5, Z=42  # liczba zadań  # liczba maszyn  # ziarno generatora
)
target = instance.calc_target

algorithm_kwargs = {
    "selection_method": "tournament",
    "mutation_method": "swap",
    "mutate_prob": 0.02,
    "p": 500,
    "iter_limit": 100,
}

# sol = ga(instance, **algorithm_kwargs)
#
# print(f"solution = {sol}")
# print(f"f(solution) = {target(sol)}")

algorithm_kwargs_list = [
    {"selection_method": "tournament", "mutation_method": "swap", "mutate_prob": 0.02, "p": 500, "iter_limit": 100},
    {"selection_method": "tournament", "mutation_method": "reinsert", "mutate_prob": 0.02, "p": 500, "iter_limit": 100},
    {"selection_method": "roulette", "mutation_method": "swap", "mutate_prob": 0.02, "p": 500, "iter_limit": 100},
    {"selection_method": "roulette", "mutation_method": "reinsert", "mutate_prob": 0.02, "p": 500, "iter_limit": 100},
]

n_repetitions = 3
results = {}

for kwargs in algorithm_kwargs_list:
    key = (kwargs['selection_method'], kwargs['mutation_method'])
    values = [target(ga(instance, **kwargs)) for _ in range(n_repetitions)]
    average_value = np.mean(values)
    results[key] = average_value

labels = [f"{sel}\n{mut}" for sel, mut in results.keys()]
values = list(results.values())

fig, ax = plt.subplots()
ax.bar(labels, values, color=['blue', 'orange', 'green', 'red'])
ax.set_xlabel('GA Configuration')
ax.set_ylabel('Average Objective Value')
ax.set_title('Comparison of GA Configurations')

plt.xticks(rotation=45)
plt.tight_layout()
plt.show()
