import numpy as np
import time
from numpy.random import uniform


def pso(
    n: int,  # Ilość zmiennych / wymiarowość
    U: np.ndarray,  # Ograniczenia górne dla każdej zmiennej
    L: np.ndarray,  # Ograniczenia dolne dla każdej zmiennej
    w: float,  # Waga inercji (spowolnienie szybkości w danym kierunku)
    phi_l: float,  # Współczynnik kognitywny (lokalny/cząstki)
    phi_g: float,  # Współczynnik społeczny (globalny)
    c: float,  # Tempo uczenia
    k: int,  # Liczba cząstek
    target: callable,  # Funkcja celu
    time_limit: int = None,  # Limit czasowy jako warunek stopu
    iteration_limit: int = None,  # Limit ilości iteracji od poprawy jako warunek stopu
) -> np.ndarray:

    # Zmiana z (n,) na (n,1)
    L, U = L[:, np.newaxis], U[:, np.newaxis]

    x = np.zeros((n, k))  # Macierz pozycji cząstek
    v = np.zeros((n, k))  # Macierz prędkości cząstek
    l = np.zeros((n, k))  # Najlepsze znane rozwiązanie lokalne dla każdej z cząstek
    g = np.zeros((n))  # Najlepsze znane globalne rozwiązanie

    # Wypełnij wszystkie x poprzez U(L,U), oraz v poprzez U(L-U,U-L)
    x = uniform(L, U, size=x.shape)
    l = np.copy(x)
    v = uniform(L - U, U - L, size=v.shape)

    # Znajdź najlepsze rozwiązanie spośród początkowych
    target_x = np.apply_along_axis(func1d=target, axis=0, arr=x)
    g = x[:, np.argmin(target_x)]

    start_time = time.time()
    iterations_since_improvement = 0

    while True:

        for j in range(k):
            for i in range(n):
                r_l = uniform(0, 1)
                r_g = uniform(0, 1)
                v[i, j] = (
                    w * v[i, j]
                    + phi_l * r_l * (l[i, j] - x[i, j])
                    + phi_g * r_g * (g[i] - x[i, j])
                )
            x[i, j] = x[i, j] + c * v[i, j]
            if target(x[:, j]) < target(l[:, j]):
                l[:, j] = x[:, j]
                if target(l[:, j]) < target(g):
                    g = l[:, j]
                    iterations_since_improvement = 0

        iterations_since_improvement += 1
        if time_limit is not None and time.time() - start_time >= time_limit:
            break
        if (
            iteration_limit is not None
            and iterations_since_improvement >= iteration_limit
        ):
            break

    return g
