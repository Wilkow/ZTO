# Problem 2.7
# Szeregowanie zadań na jednej maszynie z ważoną sumą opóźnień wiTi
import numpy as np

from Generator.RandomNumberGenerator import RandomNumberGenerator
import random
import time
from cmath import inf, e


def random_permutation(n: int):
    permutation = list(range(0, n))
    random.shuffle(permutation)
    return permutation


def calc_target(order):
    delay_sum = 0
    for i, ind in enumerate(order):
        if i > 0:
            C_pi = C_pi + p[ind]
        else:
            C_pi = p[ind]
        T_pi = max(0, C_pi - d[ind])
        delay = w[ind] * T_pi
        delay_sum += delay
    return delay_sum


def get_random_search_solution(init_solution, t=3):
    solution = [*init_solution]
    start_time = time.time()
    while True:
        swap_indexes = random.sample(range(len(init_solution)), 2)
        new_solution = [*solution]
        new_solution[swap_indexes[0]], new_solution[swap_indexes[1]] = (
            new_solution[swap_indexes[1]],
            new_solution[swap_indexes[0]],
        )
        if calc_target(new_solution) < calc_target(solution):
            solution = new_solution
        if time.time() - start_time > t:
            break
    return solution


def get_sa(init_sol, t=3):
    solution = [*init_sol]
    best_solution = [*init_sol]
    start_time = time.time()
    temp = 10000
    alpha = 0.998

    while True:

        swap_indexes = random.sample(range(len(init_sol)), 2)
        new_solution = [*solution]
        new_solution[swap_indexes[0]], new_solution[swap_indexes[1]] = (
            new_solution[swap_indexes[1]],
            new_solution[swap_indexes[0]],
        )

        if calc_target(new_solution) < calc_target(solution):
            solution = new_solution
        else:
            prob = pow(e, (-calc_target(new_solution) - calc_target(solution) / temp))
            if random.random() < prob:
                solution = new_solution
        if calc_target(solution) < calc_target(best_solution):
            best_solution = solution

        temp = alpha * temp

        if time.time() - start_time > t:
            break
    return best_solution


### generacja instancji
rng = RandomNumberGenerator(seedVaule=50)
n = 50
p = []
w = []
d = []
for i in range(1, n + 1):
    p.append(rng.nextInt(1, 30))
    w.append(rng.nextInt(1, 30))
S = sum(p)
for i in range(1, n + 1):
    d.append(rng.nextInt(1, S))
###

print(f"p = {p}")
print(f"w = {w}")
print(f"d = {d}")

init_solution = random_permutation(n)
print("---")
# print(f"initial_solution = {init_solution}")
# print(calc_target(init_solution))
#
# found_solution = get_random_search_solution(init_solution)
# found_solution_val = calc_target(found_solution)
#
# print(f"final solution = {found_solution}")
# print(found_solution_val)
#
# found_solution_2 = get_sa(init_solution)
# found_solution_2_val = calc_target(found_solution_2)
#
# print(f"SA_final solution = {found_solution_2}")
# print(found_solution_2_val)

it_r = 10

# for z in range(2):
_time = 1
print("\n---------------------")
print("RESULTS FOR n=" + str(n))
print("---------------------\n")
for j in range(5):

    better = 0
    matrix_rs = []
    matrix_sa = []

    for i in range(it_r):
        matrix_rs.append(calc_target(get_random_search_solution(init_solution, _time)))
        matrix_sa.append(calc_target(get_sa(init_solution, _time)))
    for i in range(it_r):
        if matrix_rs[i] > matrix_sa[i]:
            better += 1

    print("RS: " + str(matrix_rs))
    print("SA: " + str(matrix_sa))

    print("\nSA better than RS: " + str(better) + "/" + str(it_r) + " with function time limitation = " + str(_time) + "s\n")
    _time += 2
    # n = int(0.2*n)


"""
p = [1, 24, 20, 11, 29, 6, 17, 21, 17, 6, 27, 14, 10, 18, 24, 16, 4, 25, 19, 17, 11, 4, 18, 18, 20, 10, 27, 29, 10, 14, 7, 25, 10, 10, 12, 2, 26, 15, 2, 3, 12, 24, 13, 25, 30, 20, 17, 17, 19, 14]
w = [18, 28, 29, 29, 23, 30, 22, 15, 12, 11, 11, 16, 11, 21, 2, 4, 12, 13, 25, 9, 4, 21, 20, 8, 25, 27, 23, 10, 27, 1, 7, 29, 21, 17, 21, 7, 21, 7, 16, 16, 9, 13, 15, 9, 13, 10, 20, 9, 3, 24]
d = [684, 313, 285, 724, 542, 461, 96, 516, 571, 730, 607, 328, 577, 425, 192, 81, 781, 504, 582, 412, 435, 577, 119, 565, 272, 388, 707, 787, 36, 693, 277, 477, 409, 54, 230, 306, 780, 298, 284, 221, 400, 609, 638, 322, 131, 347, 72, 194, 479, 337]
---

---------------------
RESULTS FOR n=50
---------------------

RS: [1653, 1849, 1673, 1792, 1710, 1898, 1718, 1946, 1898, 1596]
SA: [1732, 1792, 1655, 1668, 2050, 1864, 1402, 1724, 1918, 1645]

SA better than RS: 6/10 with function time limitation = 1s

RS: [1718, 1596, 1588, 1743, 1898, 1757, 1666, 1906, 2050, 1673]
SA: [1906, 1718, 1743, 1645, 1870, 1906, 1718, 1753, 2058, 1792]

SA better than RS: 3/10 with function time limitation = 3s

RS: [1898, 1889, 1792, 1822, 1923, 1571, 1792, 1608, 1571, 1730]
SA: [1743, 1718, 2110, 1645, 1782, 1915, 2058, 1596, 1644, 1806]

SA better than RS: 5/10 with function time limitation = 5s

RS: [1792, 1743, 1886, 1898, 1792, 1743, 1596, 1718, 1596, 1812]
SA: [2058, 1775, 2348, 1800, 1743, 1964, 1792, 1767, 1648, 1822]

SA better than RS: 2/10 with function time limitation = 7s

RS: [1718, 1792, 1792, 1783, 1916, 1718, 1767, 1718, 1571, 1644]
SA: [1898, 1644, 1906, 1743, 1775, 1718, 1673, 1718, 1721, 1804]

SA better than RS: 4/10 with function time limitation = 9s

---------------------
RESULTS FOR n=10
---------------------

RS: [2336, 2313, 2336, 2336, 2336, 2313, 2313, 2304, 2336, 2336]
SA: [2304, 2336, 2313, 2336, 2505, 2313, 2336, 2313, 2336, 2313]

SA better than RS: 3/10 with function time limitation = 1s

RS: [2313, 2313, 2336, 2313, 2313, 2336, 2304, 2313, 2336, 2417]
SA: [2313, 2336, 2313, 2313, 2313, 2313, 2505, 2313, 2313, 2336]

SA better than RS: 4/10 with function time limitation = 3s

RS: [2336, 2505, 2313, 2313, 2313, 2336, 2313, 2336, 2313, 2313]
SA: [2304, 2336, 2336, 2313, 2313, 2336, 2336, 2336, 2336, 2336]

SA better than RS: 2/10 with function time limitation = 5s

RS: [2313, 2336, 2313, 2313, 2336, 2313, 2304, 2336, 2336, 2313]
SA: [2336, 2313, 2336, 2313, 2313, 2313, 2313, 2313, 2336, 2313]

SA better than RS: 3/10 with function time limitation = 7s

RS: [2313, 2336, 2336, 2304, 2336, 2313, 2336, 2336, 2336, 2313]
SA: [2336, 2336, 2336, 2336, 2477, 2336, 2304, 2336, 2336, 2304]

SA better than RS: 2/10 with function time limitation = 9s


---------------------
RESULTS FOR n=2
---------------------

RS: [2336, 2304, 2336, 2336, 2304, 2313, 2313, 2336, 2336, 2336]
SA: [2336, 2313, 2304, 2336, 2313, 2336, 2313, 2336, 2304, 2336]

SA better than RS: 2/10 with function time limitation = 1s

RS: [2336, 2313, 2336, 2336, 2313, 2313, 2313, 2313, 2336, 2313]
SA: [2313, 2313, 2336, 2304, 2313, 2336, 2313, 2313, 2313, 2336]

SA better than RS: 3/10 with function time limitation = 3s

RS: [2313, 2336, 2313, 2313, 2336, 2336, 2336, 2505, 2505, 2313]
SA: [2336, 2313, 2336, 2313, 2313, 2313, 2304, 2336, 2505, 2336]

SA better than RS: 5/10 with function time limitation = 5s

RS: [2313, 2477, 2336, 2336, 2336, 2313, 2313, 2313, 2336, 2313]
SA: [2313, 2313, 2304, 2336, 2336, 2313, 2313, 2336, 2336, 2313]

SA better than RS: 2/10 with function time limitation = 7s

RS: [2336, 2313, 2313, 2336, 2417, 2304, 2336, 2313, 2313, 2313]
SA: [2336, 2336, 2336, 2336, 2313, 2336, 2313, 2304, 2336, 2336]

SA better than RS: 3/10 with function time limitation = 9s"""