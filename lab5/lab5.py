# Problem 2.7 
# Szeregowanie zadań na jednej maszynie z ważoną sumą opóźnień wiTi
from Generator.RandomNumberGenerator import RandomNumberGenerator
import random
import time
import math
from statistics import mean
import matplotlib.pyplot as plt
import numpy as np

def random_permutation(n: int):
    permutation = list(range(0, n))
    random.shuffle(permutation) 
    return permutation

def generate_instance(seed, n):
    ### generacja instancji
    rng = RandomNumberGenerator(seedVaule=seed)
    p = []
    w = []
    d = []
    for _ in range(1,n+1):
        p.append(rng.nextInt(1,30))
        w.append(rng.nextInt(1,30))
    S = sum(p)
    for i in range(1,n+1):
        d.append(rng.nextInt(1,S))
    ###
    return p, w, d

def calc_target(order):
    delay_sum = 0
    for i, ind in enumerate(order):
        if i > 0:
            C_pi = C_pi + p[ind]
        else:
            C_pi = p[ind]
        T_pi = max(0,C_pi-d[ind])
        delay = w[ind] * T_pi
        delay_sum += delay
    return delay_sum

def swap(sol):
    swap_indexes = random.sample(range(len(sol)),2)
    new_solution = [*sol]
    new_solution[swap_indexes[0]], new_solution[swap_indexes[1]] = \
        new_solution[swap_indexes[1]], new_solution[swap_indexes[0]]
    return new_solution

def reinsert(sol: list):
    reinsert_indexes = random.sample(range(len(sol)),2)
    new_sol = [*sol]
    value = new_sol.pop(reinsert_indexes[0])
    new_sol.insert(reinsert_indexes[1], value)
    return new_sol

def get_random_search_solution(init_solution, iterations=np.inf, max_time=np.inf):
    max_iterations_for_improvement = iterations
    iterations_since_improvement = 0
    start_time = time.time()
    solution = [*init_solution]
    while True:
        new_solution = reinsert(solution)
        if calc_target(new_solution) < calc_target(solution):
            solution = new_solution
            iterations_since_improvement = 0
        else:
            iterations_since_improvement+=1
        if iterations_since_improvement>=max_iterations_for_improvement or time.time()-start_time>max_time:
            break
    return solution

def get_simulated_annealing_solution(init_solution, t ,alfa, iterations=np.inf, max_time=np.inf):
    best_solution = [*init_solution]
    current_solution = [*init_solution]
    max_iterations_for_improvement = iterations
    iterations_since_improvement = 0
    start_time = time.time()
    t = t
    alfa = alfa
    while True:
        new_solution = reinsert(current_solution)
        
        if calc_target(new_solution) < calc_target(current_solution):
            current_solution = [*new_solution]
        else:
            override_propability = math.pow(math.e,-(calc_target(new_solution)-calc_target(current_solution))/t)
            if random.choices([True,False],[override_propability, 1-override_propability])[0]:
                current_solution = [*new_solution]


        if calc_target(current_solution) < calc_target(best_solution):
            best_solution = [*current_solution]

            iterations_since_improvement = 0
        else:
            iterations_since_improvement+=1
        t = alfa*t
        if iterations_since_improvement>=max_iterations_for_improvement or time.time()-start_time>max_time:
            break
    return best_solution


# n = 10
# p, w, d = generate_instance(seed=50, n=n)
# print(f'p = {p}')
# print(f'w = {w}')
# print(f'd = {d}')
# init_solution = random_permutation(n)
# print("---")
# print(f'initial_solution = {init_solution}')
# print(calc_target(init_solution))
# found_solution = get_random_search_solution(init_solution, 
#                                             iterations=10000,
#                                             max_time=2
#                                             )
# print(f'RS solution = {found_solution}')
# print(calc_target(found_solution))
# found_solution = get_simulated_annealing_solution(init_solution, t=10, alfa=0.915, 
#                                                   iterations=10000,
#                                                   max_time=2
#                                                   )
# print(f'SA solution = {found_solution}')
# print(calc_target(found_solution))

# best_sol = init_solution
# for t in range(1,200):
#     print(t, 200)
#     for a in range(1,100):
#         sol = get_simulated_annealing_solution(init_solution, t=t*10, alfa=0.9+0.001*a, iterations=1000)
#         if calc_target(sol) < calc_target(best_sol):
#             print(t*10,0.9+0.001*a,calc_target(sol))
#             best_sol = sol

# iterations = [100,200,500,1000,2000,5000,10000]
# rs_solutions_n = []
# sa_solutions_n = []
# n_vals = [10,15,25,40, 50,75,100]
# for n in n_vals:
#     rs_solutions = []
#     sa_solutions = []
#     p, w, d = generate_instance(seed=50, n=n)
#     for iteration_val in iterations:
#         rs_solutions_tmp = []
#         sa_solutions_tmp = []
#         init_solution = random_permutation(n)
#         for run_id in range(4):
#             rs_solutions_tmp.append(calc_target(get_random_search_solution(init_solution, iterations=iteration_val)))
#             sa_solutions_tmp.append(calc_target(get_simulated_annealing_solution(init_solution, t=10, alfa=0.915, iterations=iteration_val)))
#         rs_solutions.append(mean(rs_solutions_tmp))
#         sa_solutions.append(mean(sa_solutions_tmp))
#     rs_solutions_n.append(rs_solutions)
#     sa_solutions_n.append(sa_solutions)
#     print(rs_solutions)
#     print(sa_solutions)

# for i,n in enumerate(n_vals):
#     series = [100*(rs_solutions_n[i][j]-sa_solutions_n[i][j])/sa_solutions_n[i][j] for j in range(len(rs_solutions_n[i]))]
#     print(series)
#     plt.plot(iterations, series, label=f'n={n}')

# plt.legend()
# plt.xlabel('Ilosc iteracji do poprawy')
# plt.ylabel('Roznica w wyniku [%] (rs-sa/sa)*100%')
# plt.savefig('iteration_bound_performance.png')

# max_times = [1,2,3]
# rs_solutions_n = []
# sa_solutions_n = []
# n_vals = [10,
#         #   15,
#        25,
#         #  40,
#          50,
#         #  75,
#         #  100
#           ]
# for n in n_vals:
#     rs_solutions = []
#     sa_solutions = []
#     p, w, d = generate_instance(seed=50, n=n)
#     for iteration_val in max_times:
#         rs_solutions_tmp = []
#         sa_solutions_tmp = []
#         init_solution = random_permutation(n)
#         for run_id in range(5):
#             rs_solutions_tmp.append(calc_target(get_random_search_solution(init_solution, max_time=iteration_val)))
#             sa_solutions_tmp.append(calc_target(get_simulated_annealing_solution(init_solution, t=10, alfa=0.915, max_time=iteration_val)))
#         rs_solutions.append(mean(rs_solutions_tmp))
#         sa_solutions.append(mean(sa_solutions_tmp))
#     rs_solutions_n.append(rs_solutions)
#     sa_solutions_n.append(sa_solutions)
#     print(rs_solutions)
#     print(sa_solutions)

# for i,n in enumerate(n_vals):
#     series = [100*(rs_solutions_n[i][j]-sa_solutions_n[i][j])/sa_solutions_n[i][j] for j in range(len(rs_solutions_n[i]))]
#     print(series)
#     plt.plot(max_times, series, label=f'n={n}')

# plt.legend()
# plt.xlabel('Maksymalny czas wykonania algorytmu [s]')
# plt.ylabel('Roznica w wyniku [%] (rs-sa/sa)*100%')
# plt.savefig('time_bound_performance.png')

iterations = [100,200,500,1000,2000,5000,10000]
rs_solutions_n = []
sa_solutions_n = []
n_vals = [10,15,25,40, 50,75,100]
for n in n_vals:
    rs_solutions = []
    sa_solutions = []
    p, w, d = generate_instance(seed=50, n=n)
    for iteration_val in iterations:
        rs_solutions_tmp = []
        sa_solutions_tmp = []
        init_solution = random_permutation(n)
        for run_id in range(4):
            rs_solutions_tmp.append(calc_target(get_random_search_solution(init_solution, iterations=iteration_val)))
            sa_solutions_tmp.append(calc_target(get_simulated_annealing_solution(init_solution, t=10, alfa=0.915, iterations=iteration_val)))
        rs_solutions.append(mean(rs_solutions_tmp))
        sa_solutions.append(mean(sa_solutions_tmp))
    rs_solutions_n.append(rs_solutions)
    sa_solutions_n.append(sa_solutions)
    print(rs_solutions)
    print(sa_solutions)

for i,n in enumerate(n_vals):
    series = [100*(rs_solutions_n[i][j]-sa_solutions_n[i][j])/sa_solutions_n[i][j] for j in range(len(rs_solutions_n[i]))]
    print(series)
    plt.plot(iterations, series, label=f'n={n}')

plt.legend()
plt.xlabel('Ilosc iteracji do poprawy')
plt.ylabel('Roznica w wyniku [%] (rs-sa/sa)*100%')
plt.savefig('iteration_bound_performance_insert.png')