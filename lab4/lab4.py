# Problem komiwojażera
from Generator.RandomNumberGenerator import RandomNumberGenerator
import random
from pprint import pprint
import time
from functools import wraps
import math
import matplotlib.pyplot as plt 
from statistics import mean

def timing_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Execution time of {func.__name__}: {end_time - start_time} seconds")
        return result
    return wrapper


def random_permutation(n: int) -> list[int]:
    permutation = list(range(0, n))
    random.shuffle(permutation) 
    return permutation


def min_id_excluding(lst: list[int], excluded_ids: list[int]) -> int:
    min_val = math.inf
    ind = -1
    for i in set(range(len(lst)))-set(excluded_ids):
        if lst[i] < min_val:
            min_val = lst[i]
            ind = i
    return ind


def calc_dist_from_visited(dists: list[list[int]], visited: list):
    sum = 0
    for i in range(len(visited)-1):
        sum += dists[visited[i]][visited[i+1]]
    sum+=dists[visited[-1]][visited[0]]
    return sum


def get_naive_solutions(dists: list[list[int]], n: int) -> list[list[int]]:
    solutions = []
    for start_ind in range(n):
        current_ind = start_ind
        visited = [current_ind]
        while len(visited) < n:
            next_min = min_id_excluding(dists[current_ind],visited)
            current_ind = next_min
            visited.append(current_ind)
        solutions.append(visited)
    return solutions


def get_random_solutions(n: int) -> list[list[int]]:
    solutions = []
    for _ in range(500):
        visited = random_permutation(n)
        solutions.append(visited)
    return solutions


@timing_decorator
def get_b_and_b_solution(dists: list[list[int]], n: int, ub: int) -> list[int]:
    solution: list[int] = []
    best_sol = ub
    # [([1,3,4],26), ([1,3,5],28),([1,2,3,4],51), ([1,2,3,4],51), ([1,2,3,4],51)]
    global_min = sum([min(dists[i]) for i in range(n)])
    queue: list[tuple[list[int],int]] = [([0],global_min)]
    while queue:
        element = queue.pop()
        # sprawdzenie czy jest juz lisciem 
        if len(element[0]) == n:
            calc = calc_dist_from_visited(dists, element[0])
            if calc < best_sol:
                # print(f'Path {element[0]} : dists_sum {calc}')
                solution = element[0]
                best_sol = calc
        else:
            for i in set(range(n)).difference(element[0]):
                lb = element[1]-min(dists[element[0][-1]])+dists[element[0][-1]][i]
                if lb < best_sol:
                    queue.append(([*element[0],i],lb))
    return solution


@timing_decorator
def get_bs_solution(dists: list[list[int]], n: int, ub: int, k: int) -> list[int]:
    solution: list[int] = []
    best_sol = ub
    # [([1,3,4],26), ([1,3,5],28),([1,2,3,4],51), ([1,2,3,4],51), ([1,2,3,4],51)]
    global_min = sum([min(dists[i]) for i in range(n)])
    queue: list[tuple[list[int],int]] = [([0],global_min)]
    next_queue: list[tuple[list[int],int]] = []
    while queue:
        element = queue.pop()
        # sprawdzenie czy jest juz lisciem
        if len(element[0]) == n:
            calc = calc_dist_from_visited(dists, element[0])
            if calc < best_sol:
                #print(f'Path {element[0]} : dists_sum {calc}')
                solution = element[0]
                best_sol = calc
        else:
            for i in set(range(n))-set(element[0]):
                lb = element[1]-min(dists[element[0][-1]])+dists[element[0][-1]][i]
                if lb < best_sol:
                    next_queue.append(([*element[0],i],lb))
        if not queue and next_queue:
            queue = sorted(next_queue, key=lambda x: x[1])[:k]
            next_queue = []
    return solution


MIN_DIST_VAL=1
MAX_DIST_VAL=30
SEED_VALUE=52
N = 20

# 0. Generacja instancji
# rng = RandomNumberGenerator(SEED_VALUE)
# dists: list[list[int]] = [[rng.nextInt(MIN_DIST_VAL,MAX_DIST_VAL) for _ in range(N)] for _ in range(N)]
# print(f"Adjacency matrix N={N}")
# pprint(dists)

# 1. Wyznacz jakiekolwiek rozwiazania poczatkowe, Algorytm zachlanny + Algorytm losowy
# solutions = [
#             *get_random_solutions(N), 
#             *get_naive_solutions(dists, N)
#              ]

# # 2. Ustawienie najniszej wartości z wygenerowanych rozwiązań jako UB
# upper_bound = math.inf
# for sol in solutions:
#     dist = calc_dist_from_visited(dists, sol)
#     if dist < upper_bound:
#         upper_bound = dist
# print(f'ub = {upper_bound}')

# 3. Obliczenie B&B
# print('Running B&B algorithm')
# sol = get_b_and_b_solution(dists=dists, n=N, ub=upper_bound)
# print("B&B Result:")
# print(sol)
# print(calc_dist_from_visited(dists,sol))

# # 4. Obliczenie BS
# print('Running BS algorithm')
# sol = get_bs_solution(dists=dists, n=N, ub=upper_bound, k=1500)
# print("BS Result:")
# print(sol)
# print(calc_dist_from_visited(dists,sol))

# Porownanie BB vs BS
# Procentowa roznica w wyniku
# Czas wykonania
# 

# 
results_x = []
results_y_bb = []
results_y_bs = []
results_y_bb_time = []
results_y_bs_time = []

for n in range(12,19+1):
    results_x.append(n)
    results_y_bb_tmp = []
    results_y_bs_tmp = []
    results_y_bb_time_tmp = []
    results_y_bs_time_tmp = []
    for seed in range(1,5+1):
        rng = RandomNumberGenerator(seed)
        dists: list[list[int]] = [[rng.nextInt(MIN_DIST_VAL,MAX_DIST_VAL) for _ in range(n)] for _ in range(n)]
        solutions = [
            *get_random_solutions(n), 
            *get_naive_solutions(dists, n)
             ]

        upper_bound = math.inf
        for sol in solutions:
            dist = calc_dist_from_visited(dists, sol)
            if dist < upper_bound:
                upper_bound = dist
        print(f'n={n} seed={seed}')

        start_time = time.time()
        sol = get_b_and_b_solution(dists=dists, n=n, ub=upper_bound)
        stop_time = time.time()
        exec_time = stop_time-start_time
        print(sol, calc_dist_from_visited(dists,sol))
        results_y_bb_tmp.append(calc_dist_from_visited(dists,sol))
        results_y_bb_time_tmp.append(exec_time)
        #for k in [500,1000,2000,5000]:
        #print(f'k={k}')
        start_time = time.time()
        sol = get_bs_solution(dists=dists, n=n, ub=upper_bound, k=1500)
        stop_time = time.time()
        exec_time = stop_time-start_time
        print(sol, calc_dist_from_visited(dists,sol))
        results_y_bs_tmp.append(calc_dist_from_visited(dists,sol))
        results_y_bs_time_tmp.append(exec_time)
    results_y_bb.append(mean(results_y_bb_tmp))
    results_y_bs.append(mean(results_y_bs_tmp))
    results_y_bb_time.append(mean(results_y_bb_time_tmp))
    results_y_bs_time.append(mean(results_y_bs_time_tmp))


print(results_x)
print(results_y_bb)
print(results_y_bs)
print(results_y_bb_time)
print(results_y_bs_time)
diff_results_y = [100*abs(results_y_bb[i]-results_y_bs[i])/results_y_bb[i] for i in range(len(results_x))]
diff_results_y_time = [100*abs(results_y_bb_time[i]-results_y_bs_time[i])/results_y_bs_time[i] for i in range(len(results_x))]
print(diff_results_y)
plt.plot(results_x, diff_results_y, 'r')
plt.savefig('percentage_results.png')
plt.clf()
plt.plot(results_x, diff_results_y_time, 'r')
plt.savefig('percentage_time.png')